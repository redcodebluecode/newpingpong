package com.madcoatgames.newpong.audio;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.utils.Disposable;

public class SoundMaster implements Disposable{	
	//anything can access these.
	private Sound 	jump, 	land, 	normal, 	acquire, 	enemyDead, 	heroDead, 	enemyHit, 	heroHit, 	dash,
					special,		hyper,		elec,		charge;
	
	public static boolean 	jumpq, 	landq, 	normalq,	acquireq, 	enemyDeadq, heroDeadq, 	enemyHitq, 	heroHitq, 	dashq,
					specialq,		hyperq,		elecq,		chargeq;
	public static boolean	unleashq, tokenq, fireworksq, abductionq;
	
	public static boolean landDisabled = true, dashDisabled = false, heroDeadDisabled = false, enemyDeadDisabled = false;
	
	public SoundMaster(){
		loadPlay();
		resetPlay();
	}
	
	private void loadPlay(){
		jump 		= Gdx.audio.newSound(Gdx.files.internal	("sound/Jump4.wav")); //paddle not powerup
		land 		= Gdx.audio.newSound(Gdx.files.internal	("sound/Hit_Hurt6.wav"));
		normal 		= Gdx.audio.newSound(Gdx.files.internal	("sound/Laser_Shoot8.wav"));//ping pong hit wall vert
		acquire 	= Gdx.audio.newSound(Gdx.files.internal	("sound/Pickup_Coin24.wav"));
		enemyDead 	= Gdx.audio.newSound(Gdx.files.internal	("sound/Explosion11.wav"));
		heroDead 	= Gdx.audio.newSound(Gdx.files.internal	("sound/Powerup.wav"));
		enemyHit 	= Gdx.audio.newSound(Gdx.files.internal	("sound/Explosion15.wav"));
		heroHit 	= Gdx.audio.newSound(Gdx.files.internal	("sound/Pickup_Coin10.wav")); //lose juice
		dash		= Gdx.audio.newSound(Gdx.files.internal	("sound/Powerup2.wav"));
		special		= Gdx.audio.newSound(Gdx.files.internal	("sound/Laser_Shoot9.wav")); //acquired star //hit paddle
		hyper		= Gdx.audio.newSound(Gdx.files.internal	("sound/Laser_Shoot21.wav")); //hyper star
		elec		= Gdx.audio.newSound(Gdx.files.internal	("sound/Laser_Shoot24.wav"));
		charge		= Gdx.audio.newSound(Gdx.files.internal	("sound/Laser_Shoot19.wav"));
	}
	public void disposePlay(){
		jump.dispose();
		land.dispose();
		normal.dispose();
		acquire.dispose();
		enemyDead.dispose();
		heroDead.dispose();
		enemyHit.dispose();
		heroDead.dispose();
		dash.dispose();
		special.dispose();
		hyper.dispose();
		elec.dispose();
		charge.dispose();
	}
	public void update(){
		playOnQueue();
		resetPlay();
	}
	public void playOnQueue(){
		if 	(jumpq) 			jump.play();
		if 	(landq && !landDisabled)	{
								land.play();
								landDisabled = true;
		}
		if	(hyperq)			{
								hyper.stop();
								hyper.play();
								specialq = false;
								normalq = false;
		}
		if 	(specialq)			{
								special.stop();
								special.play();
								normalq = false;
		}
		if 	(normalq) 			{
								normal.stop();
								normal.play();
		}
		
		
		if 	(acquireq) 			{
								enemyHitq = false;
								acquire.play();
		}
		if 	(enemyDeadq)		{
								enemyDead.stop();
								enemyDead.play();
		}
		if 	(heroDeadq && !heroDeadDisabled) {
								heroDead.play();
								heroDeadDisabled = true;
		}
		if 	(enemyHitq)			{
								enemyHit.stop();
								enemyHit.play();
		}
		if 	(heroHitq) 			heroHit.play();
		if 	(dashq && !dashDisabled)	{
								dash.play();
								dashDisabled = true;
		}
		if	(elecq)				{
								elec.stop();
								elec.play();
		}
		if	(chargeq)			{
								charge.stop();
								charge.play();
		}
		if	(unleashq)			{
								charge.stop();
								hyper.play(1, .3f, 0);
		}
		if 	(tokenq)			acquire.play(1, 1.5f, 0);
		if 	(fireworksq)		{
								heroHit.stop();
								heroHit.play(1, .65f, 0);
								//fireworks.play(1, .5f, 0);
		}
		if 	(abductionq)		{
								heroDead.stop();
								heroDead.play(1, 1.5f, 0);
								//fireworks.play(1, .5f, 0);
		}
	}
	private void resetPlay(){
		jumpq 		= false;
		landq 		= false;
		normalq 	= false;
		acquireq 	= false;
		enemyDeadq 	= false;
		heroDeadq	= false;
		enemyHitq 	= false;
		heroHitq 	= false;
		dashq 		= false;
		specialq 	= false;
		hyperq 		= false;
		elecq 		= false;
		chargeq 	= false;
		
		unleashq	= false;
		tokenq 		= false;
		fireworksq 	= false;
		abductionq 	= false;
	}

	@Override
	public void dispose() {
		disposePlay();
	}

}
