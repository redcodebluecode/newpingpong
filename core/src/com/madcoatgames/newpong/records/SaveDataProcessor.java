package com.madcoatgames.newpong.records;

import java.io.File;
import java.io.IOException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.SerializationException;
import com.madcoatgames.newpong.NewPong;

public class SaveDataProcessor {
	public static void processToFile(SaveData saveData){
		Array<Score> scores = SaveDataCache.getScores();
		FileHandle file;
		System.out.println(Gdx.files.getExternalStoragePath());
		file = Gdx.files.external("MadCoatPong/gamedata/" + NewPong.jsonFile + ".txt");
		file.parent().mkdirs();
		file.delete();
		if (!file.exists()){
			System.out.println("file no exist");
			File maker = file.file();
			try {
				maker.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			file = new FileHandle(maker);
		}
		
		saveData.scores.addAll(scores);
		saveData.scores.sort();
		
		Json json = new Json();
		String saveScores = json.toJson(saveData);
		file.writeString(saveScores, false);
		if (!file.exists()){
			System.out.println("file still no exist");
		} else {
			System.out.println(file.readString());
		}
		System.out.println(file.path());
	}
	public static Array<Score> generateScores(){
		Array<Score> scores;
		SaveData saveData;
		
		Json json = new Json();
		FileHandle file = Gdx.files.external("MadCoatPong/gamedata/" + NewPong.jsonFile + ".txt");
		file.parent().mkdirs();//turns the slashes into directories, still not a file
		System.out.println("external storage available: " + Gdx.files.isExternalStorageAvailable());
		System.out.println(Gdx.files.getExternalStoragePath());
		
		if (!file.exists()){
			System.out.println("file doesn't exist");
			FileHandle tempPath = Gdx.files.external("MadCoatPong/gamedata/");
			tempPath.mkdirs();
			tempPath.file();
			File temp = new File(tempPath.file(), NewPong.jsonFile + ".txt");
			//File temp = new File(NewPong.jsonFile + ".txt");
			try {
				temp.createNewFile();
				file = new FileHandle(temp);
				SaveData errorData = new SaveData();
				String errorString = json.toJson(errorData); //this will write an empty (default) Json file with no game data
				file.writeString(errorString, false);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		String data = file.readString();
		System.out.println(file.readString());
		saveData = json.fromJson(
				SaveData.class
				, data);
		
		if (saveData.scores != null) {
			scores = saveData.scores;
		} else {
			scores = new Array<Score>();
		}
		/*
		try {
			saveData = json.fromJson(
					SaveData.class
					, file);
			if (saveData.scores != null) {
				scores = saveData.scores;
			} else {
				scores = new Array<Score>();
			}
		} catch (SerializationException e){
			//saveData = new SaveData();
			//json.toJson(saveData, file);
			//SaveData saveData2 = json.fromJson(SaveData.class, file);
			//scores = saveData2.scores;
			//scores.add(new Score(100, "testscore"));
			//scores = new Array<Score>();
			System.out.println("bad file");
			scores = new Array<Score>();
		}
		*/
		
		return scores;
	}

}
