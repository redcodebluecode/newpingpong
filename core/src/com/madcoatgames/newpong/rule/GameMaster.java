package com.madcoatgames.newpong.rule;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.madcoatgames.newpong.NewPong;
import com.madcoatgames.newpong.audio.MusicMaster;
import com.madcoatgames.newpong.audio.SoundMaster;
import com.madcoatgames.newpong.look.HUDRenderer;
import com.madcoatgames.newpong.look.MenuOperator;
import com.madcoatgames.newpong.look.RenderMaster;
import com.madcoatgames.newpong.look.TextureManager;
import com.madcoatgames.newpong.records.SaveDataCache;
import com.madcoatgames.newpong.records.SaveDataProcessor;
import com.madcoatgames.newpong.util.Global;
import com.madcoatgames.newpong.util.StateUpdateable;
import com.madcoatgames.newpong.util.TriColorChanger;

public class GameMaster extends ScreenMaster{
	protected Game game;
	private ShapeRenderer shaper;
	private OrthographicCamera cam;
	private SpriteBatch batch;
	private LogicMaster logic;
	private RenderMaster rm;
	private TextureManager textureManager;
	
	private MusicMaster mm;
	private SoundMaster sm;
	private HUDRenderer hr;
	
	private SaveDataCache saveDataCache;
	private SaveDataProcessor saveDataProcessor;
	
	private StarBackgroundMaster starBg;
	
	private boolean renderTextures = false;
	
	public GameMaster (Game game, MusicMaster mm){
		this.game = game;
		
		shaper = new ShapeRenderer();
		batch = new SpriteBatch();
		cam = new OrthographicCamera();
		rm = new RenderMaster();
		logic = new LogicMaster();
//		mm = new MusicMaster();
		this.mm = mm;
		mm.stopAndLoadNewTrack(MusicMaster.Track.PLAY);
		sm = new SoundMaster();
		hr = new HUDRenderer();
		textureManager = new TextureManager();
		saveDataCache = new SaveDataCache();
		saveDataProcessor = new SaveDataProcessor();
		
		cam.setToOrtho(false, (int)Global.width(), (int)Global.height());
		shaper.setProjectionMatrix(cam.combined);
		batch.setProjectionMatrix(cam.combined);
	}
	@Override
	public void dispose(){
		rm.dispose();
		shaper.dispose();
		batch.dispose();
		mm.getMusic().stop();
		mm.dispose();
		sm.dispose();
		hr.dispose();
	}
	@Override
	public void render(float delta) {
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		cam.update();
		if (MenuOperator.getType() == MenuOperator.PLAY) {
			logic.update(delta);
		} else if (MenuOperator.getType() == MenuOperator.GAMEOVER){
			logic.updatePaused(delta, hr);
			if (MenuOperator.firstGameOverCheck()) {
				StateUpdateable st = (StateUpdateable)(game);
				st.onGameOver();
			}
		}
		rm.getBackgroundMaster().setColor(logic.getTcc().c2);
		rm.drawBackground(batch);
		//rm.renderFilledBackground(shaper, logic.getTcc().c2);
		
		if (renderTextures) {
			batch.begin();
			rm.renderTextures(batch, logic.getTextureRenderables());
			batch.end();
		}
		rm.renderFilled(shaper, logic.getFilled());
		rm.renderLine(shaper, logic.getLine());
		//rm.renderShapeEnemies(shaper, logic.getEnemyMaster().getEnemies());
		if (Global.getGameMode() != Global.ARCADE) {
			rm.renderBatchEnemies(batch, logic.getEnemyMaster().getEnemies());
			rm.renderHazards(shaper, logic.getEnemyMaster().getHazards());
			rm.renderHealth(shaper, logic.getTcc(), logic.getHealth(), logic.getMaxHealth(), logic.isHit());
		}
		
		sm.update();
		hr.draw(logic.getTcc(), batch, shaper, BallPaddleMaster.getNumHits());
		
		Gdx.gl.glDisable(GL20.GL_BLEND);
		
		mm.update(delta);
	}

	@Override
	public void resize(int width, int height) {
		cam.viewportWidth = (int)Global.width();
		cam.viewportHeight = (int)Global.height();
	}

	@Override
	public void show() {
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		SaveDataProcessor.processToFile(((NewPong) game).getSaveData());
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}
	public void save(){
		SaveDataProcessor.processToFile(((NewPong) game).getSaveData());
	}
	public TriColorChanger getTriColorChanger(){
		return logic.getTcc();
	}
	public StarBackgroundMaster getStarBg() {
		return this.starBg;
	}
	public void setStarBg(StarBackgroundMaster starBg) {
		this.starBg = starBg;
		logic.setStarBg(this.starBg);
	}
}

